@extends('layouts.app')
@section('content')

<div class="row">
        <div class="col-sm-12">
            <div class="full-right">
                <center><h2>Create</h2></center>
            </div>
        </div>
    </div>
    <br>

  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      {{ Form::open(['route'=>'posts.store', 'method'=>'POST']) }}
        @include('posts.form_master')
      {{ form::close() }}
    </div>
  </div>
@endsection