@extends('layouts.app')
@section('content')

<div class="row">
        <div class="col-sm-12">
            <div class="full-right">
                <center><h2>Edit</h2></center>
            </div>
        </div>
    </div>
    <br>
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{$message}}</p>
    </div>
    @endif

  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      {{ Form::model($post,['route'=>['posts.update',$post->id],'method'=>'PATCH']) }}
      @include('posts.form_master')
      {{ Form::close() }}
    </div>
  </div>
@endsection