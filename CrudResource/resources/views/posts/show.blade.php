@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Show Price</h2>
        </div>
        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name : </strong>
            {{ $post->name}}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Price : </strong>
            {{ $post->price}}
        </div>
    </div>
</div>

<div class="pull-right">
            <br/>
            <a class="btn btn-primary" href="{{ route('posts.index') }}"> Back</a>
        </div>
@endsection
