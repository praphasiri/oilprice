@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="full-right">
                <center><h2>Oil Price</h2></center>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{$message}}</p>
    </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th width="80px"> No </th>
            <th>Name</th>
            <th>Price</th>
            <th class="text-center">
                <a href="{{ route('posts.create')}}" class="btn btn-success btn-sm">
                    Create
                </a>
            </th>
        </tr>
        <?php $no=1; ?>
        @foreach($post as $key => $value)
            <tr>
                <td>{{$no++}}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->price }}</td>
                <td class="text-center">
                    <a class="btn btn-info btn-sm" href="{{ route('posts.show',$value->id)}}">
                        Show
                    </a>
                    <a class="btn btn-primary btn-sm" href="{{route('posts.edit',$value->id)}}">
                        Edit
                    </a>
                    {!! Form::open(['method' => 'DELETE','route' => ['posts.destroy', $value->id],'style'=>'display:inline']) !!}
                        <button type="submit" style="display: inline;" class="btn btn-danger btn-sm">Delete</button>
                    {!! Form::close() !!}

                </td>
            </tr>
        @endforeach
    </table>
@endsection